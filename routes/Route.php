<?php


namespace routes;


class Route
{
    static $array = array();
    static $namePat;
    static $uriPattern;

    public static  function getURI()
    {
        if (!empty($_SERVER['REQUEST_URI'])){
        return trim($_SERVER['REQUEST_URI']);
        }
    }


    public function get($uri, $controllerAction, $nameURI = null, $puth = null)
    {
        self::$array[] = array($uri, $controllerAction, $nameURI, $puth);
    }



    public static function RouteName($name, $first = null, $second = null, $third = null)
    {
            foreach (self::$array as $items) {
                if($items[2] == $name){
                    if(!$first) {
                        $a = preg_replace('/\{.*?\}/', '', $items[0]);
                        return $a;
                    }
                    $a = preg_replace('/\{.*?\}/', '', $items[0]);
                    $slash = '/';
                    if($first){
                        $url = $a.$first;
                    }
                    if($first and $second){
                        $url = $a.$first.$slash.$second;
                    }
                    if($first and $second and $third){
                        $url = $a.$first.$slash.$second.$slash.$third;
                    }

                    return $url;
            }
        }
    }


    public static function routing(){

        foreach (self::$array as $item) {
            $ar = array("}","{");
            $ad = str_replace($ar, '', $item[0]);
            $argument = null;
            if (preg_match("~^$ad$~", self::getURI(), $u)) {

                $argument_2 = $u[1];
                $argument = array();
                if ($argument_2) {
                    for ($i = 1; $i < count($u); $i++) {
                        $argument[] = $u[$i];
                    }
                } else $argument = $u[1];
            }

            if (preg_match("~^$ad$~", self::getURI(), $a)) {
                $controllerAction = explode('@', $item[1]);
                $puth = '\app\controller\\';
                if($item[2] == 'api') $puth = '\app\api\\';
                $controllerName = $puth . $controllerAction[0];
                $actionName = $controllerAction[1];
                $controllerObject = new $controllerName($argument);
                $controllerObject->$actionName();
            }
        }
    }

    public function run()
    {
        include ('web.php');
        self::routing();
    }

}