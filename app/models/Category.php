<?php


namespace app\models;

use app\core\Model;
use \PDO;
use components\Db;

class Category
{

    public static function categories()
    {

        $db = Db::getConnection();
        $sql = 'SELECT * FROM categories';
        $result = $db->prepare($sql);
        $result->execute();
        $array = $result->fetchAll(PDO::FETCH_ASSOC);
        if($array) {
            return $array;
        }

    }

}