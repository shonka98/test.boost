<?php
namespace app\controller;


use app\models\Category;


class MainController
{

    public function action()
    {
        $result = Category::categories();
        $result = $this->arraySet($result);
        view('index', ['array' => $result]);
    }

    private function arraySet($result)
    {
        $arr_cat = array();
        for($i = 0; $i < count($result);$i++) {
            foreach($result as $value){
                $row = $value;
                    if(empty($arr_cat[$row['parent_id']])) {
                        $arr_cat[$row['parent_id']] = array();
                    }
                $arr_cat[$row['parent_id']][] = $row;
            }
            return $arr_cat;
        }
    }

}

